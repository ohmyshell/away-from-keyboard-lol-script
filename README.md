# Away-From-Keyboard LOL Script

This script is convenient when you need to leave a game without getting a cooldown timer.
It will randomly click anywhere within the fountain as well as let your team know you're afk.
---
Numpad:
1. Activate Script
2. Pause Script
3. Terminate Script
4. Show Number of Active Threads (Dev)

## Requirements
 * [Python](www.python.org)
 * [PyHook](https://www.lfd.uci.edu/~gohlke/pythonlibs/#pyhook)
 * [pyautogui](https://pyautogui.readthedocs.io/en/latest/install.html)





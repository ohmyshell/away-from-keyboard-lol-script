import pyautogui
from pythoncom import PumpMessages
import pyHook
import sys
import threading
import time
import random

res = pyautogui.size()

class Listener:
    def __init__(self):
        self.hm = pyHook.HookManager()
        self.hm.KeyDown = self.listen
        self.hm.HookKeyboard()
        self.typer = Typer()
        print("Listening")
        PumpMessages()

    def listen(self, event):
        if event.Key == 'Numpad2':
            print("Cancelled")
            self.typer.started = False
        if event.Key == 'Numpad1':
            print("Started")
            self.typer.started = True
            if self.typer.typing_thread == None:
                self.typer.start()
            else:
                self.typer.restart()
        if event.Key == 'Numpad3':
            print("Exit")
            sys.exit()
        if event.Key == 'Numpad4':
            print("Active Threads: " + str(threading.active_count()))
        return True


class Typer:
    def __init__(self):
        self.started = False
        self.lock = threading.Condition(threading.Lock())
        self.typing_thread = None

    def _start(self):
        self.started = True
        with self.lock:
            while self.started:
                coords = (random.randint(res[0]/2 - 250, res[0]/2 + 250), random.randint(res[1]/2-250, res[1]/2+250))
                print("Typing afk!")
                pyautogui.press('enter')
                pyautogui.typewrite("This is an automated message. I am AFK!", interval=0.001)
                pyautogui.press('enter')
                pyautogui.mouseDown(x=coords[0], y=coords[1], button='right')
                pyautogui.mouseUp(x=coords[0], y=coords[1], button='right')
                print("Click")
                time.sleep(1)

    def start(self):
        self.typing_thread = threading.Thread(target=self._start, name="Typer")
        self.typing_thread.start()

    def restart(self):
        if threading.active_count() < 2:
            self.start()


def main():
    listener = Listener()

if __name__ == '__main__':
    main()
